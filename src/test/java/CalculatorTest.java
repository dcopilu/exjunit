import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {

        @Test
            public void CalcSum ()

        {
            Calculator calc = new Calculator();

            Double result = calc.compute(2, 3, "+");


            Assert.assertEquals(5, result, 0);

        }


         @Test
             public void CalcSum2()

        {
            Calculator calc = new Calculator();
            Double result = calc.compute(2,-3,"+");


            Assert.assertEquals(-1, result, 0);

        }

        @Test
            public void CalcDivide()

        {
            Calculator calc = new Calculator();
            Double result = calc.compute(3,3,"/");


            Assert.assertEquals(1, result, 0);

        }

        @Test(expected = IllegalArgumentException.class)
            public void CalcDivide2()

        {
            Calculator calc = new Calculator();
            Double result = calc.compute(3,-1,"/");

        }

        @Test(expected = IllegalArgumentException.class)
            public void CalcMultiplication()

        {
            Calculator calc = new Calculator();
            Double result = calc.compute(3,-3,"*");

        }

        @Test
            public void CalcMinus()

        {
            Calculator calc = new Calculator();
            Double result = calc.compute(5,3,"-");


            Assert.assertEquals(2, result, 0);

        }

        @Test
            public void CalcMultiplicate()

        {
            Calculator calc = new Calculator();
            Double result = calc.compute(5,3,"*");


            Assert.assertEquals(15, result, 0);

        }

        @Test
            public void CalcMinus1()

        {
            Calculator calc = new Calculator();
            Double result = calc.compute(5,-3,"-");


            Assert.assertEquals(8, result, 0);

        }

        @Test(expected = IllegalArgumentException.class)
            public void CalcDivide0()

        {
            Calculator calc = new Calculator();
            Double result = calc.compute(5,0,"/");


        }

        @Test(expected = IllegalArgumentException.class)
            public void CalcOperator()

        {
            Calculator calc = new Calculator();
            calc.compute(5,1,")");
        }

        @Test
            public void CalcSqrt()

        {

            Calculator calc = new Calculator();
            Double result = calc.compute(9,1,"SQRT");

            Assert.assertEquals(3, result, 0);

        }
        @Test(expected = IllegalArgumentException.class)
            public void CalcSqrt2()

        {
            Calculator calc = new Calculator();
            calc.compute(-4,1,"SQRT");
        }


}
